import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class LoadTesterUI {
	private JFrame frame;
	private JComboBox<String> loadList;
	private JButton starter;
	private JTextField serverAddressField;
	private JTextField portAddressField;
	public LoadTesterUI(){
		frame = new JFrame("Skirmish | Load Tester");
		
		setupFrame();
	}
	
	public void display(){
		frame.setVisible(true);
	}
	private void setupFrame() {
		frame.setContentPane(createCanvas());
		frame.pack();
		frame.setResizable(false);
		frame.setLocationRelativeTo(null);
	}

	private JPanel createCanvas() {
		JPanel canvas = new JPanel();
		canvas.setPreferredSize(new Dimension(250,150));
		JLabel serverAddress = new JLabel("Server address:");
		JLabel serverPort = new JLabel("Server port: ");
		JLabel amtOfLoad = new JLabel("Load amount: ");
		
		serverAddressField = new JTextField("localhost",10);
		portAddressField = new JTextField("6969",6);
		
		String[] packetLoad = {"x50", "x100", "x1000"};
		loadList = new JComboBox<String>(packetLoad);
		JPanel a = Utils.wrap(serverAddress,serverAddressField);
		
		JPanel b = Utils.wrap(serverPort,portAddressField);
		
		JPanel c = Utils.wrap(amtOfLoad, loadList);
		
		canvas.add(a);
		canvas.add(b);
		canvas.add(c);
		
		starter = new JButton("START");
		canvas.add(starter);
		
		
		return canvas;
	}

	public void setupStartButton(ActionListener actionListener) {
		starter.addActionListener(actionListener);
		
	}

	public void setupComboBoxListener(ActionListener actionListener) {
		loadList.addActionListener(actionListener);
		
	}

	public String getServerAddress() {
		return serverAddressField.getText();
	}

	public String getServerPort() {
		return portAddressField.getText();
	}
	
	
	
}
