

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;

import javax.swing.JOptionPane;
import javax.swing.JPanel;

import exceptions.InvalidPortException;
import exceptions.InvalidServerAddressException;

public final class Utils {
	
	public static JPanel createEmptyPanel(int width, int height, Color background){
		JPanel empty = new JPanel();
		empty.setPreferredSize(new Dimension(width,height));
		empty.setBackground(background);
		return empty;
	}
	
	public static JPanel wrap(Component... components){
		JPanel panel = new JPanel();
		for(Component component : components){
			panel.add(component);
		}
		return panel;
	}
	
	public static void popupError(String message){
		JOptionPane.showMessageDialog(null, message, "ERROR!", JOptionPane.ERROR_MESSAGE);
	}
	
	public static void popupInformation(String message){
		JOptionPane.showMessageDialog(null, message, "FYI!", JOptionPane.INFORMATION_MESSAGE);
	}
	
	public static String askAddress(){
		String address = "";
		while(address == ""){
			address = JOptionPane.showInputDialog(null, "Server address: ", "SKIRMISH | Load Tester", JOptionPane.QUESTION_MESSAGE);
			if(address == null){
				return null;
			}else{
				try{
					Validate.checkAddress(address);
				}catch(InvalidServerAddressException iae){
					Utils.popupError(iae.getMessage());
					address = "";
				}
			}
		}
		return address;
	}
	
	public static int askPort(){
		int port = 0;
		while(port == 0){
			String portStr = JOptionPane.showInputDialog(null, "Port: ", "SKIRMISH | Load Tester", JOptionPane.QUESTION_MESSAGE);
			if(portStr == null){
				return 0;
			}else{
				try{
					port = Validate.port(portStr);
				}catch(InvalidPortException ipe){
					Utils.popupError(ipe.getMessage());
					port = 0;
				}
			}
		}
		return port;
	}
}
