package exceptions;

public class PacketReceiveException extends RuntimeException {

	private static final long serialVersionUID = 7414126069595829457L;

	public PacketReceiveException() {
		
	}

	public PacketReceiveException(String message) {
		super(message);
		
	}

	public PacketReceiveException(Throwable cause) {
		super(cause);
		
	}

	public PacketReceiveException(String message, Throwable cause) {
		super(message, cause);
		
	}

	public PacketReceiveException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		
	}

}
