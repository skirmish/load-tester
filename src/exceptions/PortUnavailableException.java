package exceptions;

public class PortUnavailableException extends RuntimeException {

	public PortUnavailableException() {
		
	}

	public PortUnavailableException(String message) {
		super(message);
		
	}

	public PortUnavailableException(Throwable cause) {
		super(cause);
		
	}

	public PortUnavailableException(String message, Throwable cause) {
		super(message, cause);
		
	}

	public PortUnavailableException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		
	}

}
