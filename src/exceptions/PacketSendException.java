package exceptions;

public class PacketSendException extends RuntimeException {

	private static final long serialVersionUID = -1337524246571430254L;

	public PacketSendException() {
		
	}

	public PacketSendException(String message) {
		super(message);
		
	}

	public PacketSendException(Throwable cause) {
		super(cause);
		
	}

	public PacketSendException(String message, Throwable cause) {
		super(message, cause);
		
	}

	public PacketSendException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		
	}

}
