package exceptions;

public class InvalidServerAddressException extends RuntimeException {

	private static final long serialVersionUID = 851698500935716344L;

	public InvalidServerAddressException() {
		
	}

	public InvalidServerAddressException(String message) {
		super(message);
		
	}

	public InvalidServerAddressException(Throwable cause) {
		super(cause);
		
	}

	public InvalidServerAddressException(String message, Throwable cause) {
		super(message, cause);
		
	}

	public InvalidServerAddressException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		
	}

}
