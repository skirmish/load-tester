

import exceptions.InvalidPortException;
import exceptions.InvalidServerAddressException;
import exceptions.InvalidUsernameException;

public final class Validate {
	public static int port(String port){
		
		int portValue = 0;
		try{
			portValue = Integer.parseInt(port);
		}catch(NumberFormatException nfe){
			throw new InvalidPortException("Port must be an integer!");
		}
		
		if(portValue < 1025 || portValue > 65536){
			throw new InvalidPortException("Port must be in the interval (1024, 65536)");
		}
		
		return portValue;
	}
	
	public static void checkUsername(String username){
		if(username.length() < 4){
			throw new InvalidUsernameException("Username must be at least four characters!");
		}
		
		if(username.length() > 15){
			throw new InvalidUsernameException("Username can only be at most 15 characters!");
		}
		
		if(!username.matches("(^[a-zA-Z0-9]+$)")){
			throw new InvalidUsernameException("Username must contain alphanumeric characters only!");
		}
		
		switch(username){
			case "username": case "USERNAME": 
			case "user": case "USER": case "SKIRMISH":
			case "admin": case "ADMIN": case "skirmish":
			case "administrator": case "ADMINISTRATOR":
				throw new InvalidUsernameException("You can think of a better username, right?");
		}
	}

	public static void checkAddress(String address) {
		if(address.equalsIgnoreCase("localhost")){
			return;
		}
		if(!address.matches("^\\d\\d?\\d?\\.\\d\\d?\\d?\\.\\d\\d?\\d?.\\d\\d?\\d?$")){
			throw new InvalidServerAddressException("Invalid server address!");
		}
	}
	
}
