import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;

import javax.swing.JButton;
import javax.swing.JComboBox;

import exceptions.PortUnavailableException;

public class LoadTester {
	/* SOCKET PROGRAMMING ATTRIBUTES */
	private final DatagramSocket loadTesterSocket;
	
	/* GUI ATTRIBUTES*/
	private final LoadTesterUI loadTesterUI;
	
	private int packetsPerSecond = 1;
	
	private boolean running = false;
	
	private Thread flooderThread;
	
	public LoadTester(int port){
		loadTesterUI = new LoadTesterUI();
		setupLoadTesterUI();
		try {
			loadTesterSocket = new DatagramSocket(port);
			display();				
		} catch (SocketException e) {
			throw new PortUnavailableException("Cannot listen to port "+port+".");
		}
	}
	
	private void setupLoadTesterThread(final String serverAddress, final int port){
		try {
			 final InetAddress target = InetAddress.getByName(serverAddress);
			 flooderThread = new Thread(new Runnable(){
					@Override
					public void run(){
						running = true;
						while(running){
							for(int i=0; i<packetsPerSecond; i++){
								send(target, port, "random");
							}
							try {
								Thread.sleep(1000);
							} catch (InterruptedException e) {}
						}
					}
				});
		} catch (UnknownHostException e1) {
			e1.printStackTrace();
		}
		
	}
	
	private void setupLoadTesterUI(){
		loadTesterUI.setupStartButton(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				JButton source = (JButton)e.getSource();
				String s = source.getText();
				
				switch(s){
					case "START": 
						String address = loadTesterUI.getServerAddress();
						String portStr = loadTesterUI.getServerPort();
						int port = 0;
						try{
							Validate.checkAddress(address);
							port = Validate.port(portStr);
							setupLoadTesterThread(address, port);
							flooderThread.start();
							source.setText("STOP");
						}catch(Exception ee){
							Utils.popupError(ee.getMessage());
						}
						
						break;
					case "STOP":
						running = false;
						flooderThread.interrupt();
						source.setText("START");
						break;
				}				
			}
		});
		
		loadTesterUI.setupComboBoxListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				JComboBox source = (JComboBox)e.getSource();
				String chosen = (String)source.getSelectedItem();
				
				switch(chosen){
					case "x50": packetsPerSecond = 50; break;
					case "x100": packetsPerSecond = 100; break;
					case "x1000": packetsPerSecond = 1000; break;
					default: packetsPerSecond = 1;
				}
			}
		});
	}
	
	private void display() {
		loadTesterUI.display();
		
	}
	
	//send data to client
	public void send(InetAddress address, int port, String message){
		byte[] messageBuff = message.getBytes();
		DatagramPacket packet = new DatagramPacket(messageBuff, 
									messageBuff.length,
									address, port);
		try {
			loadTesterSocket.send(packet);
			
		} catch (IOException e) {
			
		}
	}
	
	public int getPacketsPerSecond(){
		return packetsPerSecond;
	}
	
	public boolean isRunning(){
		return running;
	}
}
